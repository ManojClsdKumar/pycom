
function messageDistributor(io){

    this.io = io;

    this.broadcastAll = (tag , data) => {
        this.io.emit(tag, data, { for: 'everyone' });
    }

    this.setConnectListener = (commands) => {
        io.on('connect',(socket) => {
            console.log("new client");
            commands(socket);
        })
    }

}

module.exports = {messageDistributor};